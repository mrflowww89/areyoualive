#!/usr/bin/env bash

#########################################################################
# Description                                                           #
#     Check Port on IP and Update DNS                                   #
# V.1   Florian                                                       #
#########################################################################

# ================================================
# FUNCTION : _usage
# ================================================
_usage() {
    cat <<EOF
Usage: $0 -ip IP Adress -p Port [-h]
Options:
    -i, --ip        IP Address 
    -p, --port      Port
    -s, --source    Source DNS entry
    -d, --dest      Destination DNS entry
    -h, --help      Help
EOF
}


# Script options
SHORTOPTS='i:p:s:d:h'
LONGOPTS='ip:,port:,source:,dest:,help'
# File Zone
FILE='/var/lib/bind/db.vpn.mondomain.tld'

# ================================================
# MAIN
# ================================================

# Get arguments
ARGS=$( getopt --options $SHORTOPTS --long $LONGOPTS -- "$@" 2>/dev/null )
[ $? -ne 0 ] && ( usage ; exit 1 )
eval set -- "$ARGS"

while true ; do
    case "$1" in
        -i|--ip)        IP="$2" 	; shift 2 ;;
        -p|--port)      PORT="$2"  	; shift 2 ;;
		-s|--source)	SOURCE="$2" ; shift 2 ;;
		-d|--dest)	    DEST="$2"   ; shift 2 ;;
        -h|--help)      _usage 		; exit 0 ;;
        --)             shift ; break ;;
        *)              shift ; break ;;
    esac
done

# Required values
if [[ ${IP} == '' || ${PORT} == '' ]]; then
    _usage
    exit 1
fi

# Check the varaible is an IP address
if ! [ $( echo $IP | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b") ] ; then _usage ; exit 1 ; fi

# Check the variable is an integer
if ! let ${PORT} 2>/dev/null ; then _usage ; exit 1 ; fi

# Connexion Test
# Port Close
if ! [ $(nmap -p $PORT $IP | grep -oE open) 2>/dev/null ] ; then 

	if ! [[ $(grep ${DEST}$ ${FILE}) ]] ; then
	    # Replace CNAME 
	    sed -i -e "s/\(^${SOURCE:3:2}[^a-z-].*\)${SOURCE}/\1${DEST}/g" ${FILE}
	    # Serial Number incrementation
	    OLDSERIAL=$(grep Serial ${FILE} | awk '{print $1}')
	    NEWSERIAL=$((OLDSERIAL+1))  
	    sed -i -e "6 s/${OLDSERIAL}/${NEWSERIAL}/g" ${FILE}
	    cat ${FILE}
	    systemctl restart bind9
	else
		exit 1
	fi

# Port Open
else 
	if ! [[ $(grep ${SOURCE}$ ${FILE}) ]] ; then
	    sed -i -e "s/\(^${SOURCE:3:2}[^a-z-].*\)${DEST}/\1${SOURCE}/g" ${FILE}
	    OLDSERIAL=$(grep Serial ${FILE} | awk '{print $1}')
	    NEWSERIAL=$((OLDSERIAL+1))  
	    sed -i -e "6 s/${OLDSERIAL}/${NEWSERIAL}/g" ${FILE}
	    cat ${FILE}
	    systemctl restart bind9
	else
		exit 1
	fi
fi

